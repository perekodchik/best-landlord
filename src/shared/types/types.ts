export type Currency = 'RUB' | 'EUR' | 'USD';

export type MoneyAmount = {
  currency: Currency;
  amount: number;
};

import { PaletteOptions } from '@mui/material';

export const paletteOptions: PaletteOptions = {
  primary: {
    main: '#0BF727',
    light: '#0BF727',
    dark: '#00D877'
  },
  secondary: {
    main: '#0072FF',
    light: '#10B0F6',
    dark: '#10B0F6'
  },
  common: {
    black: '#161616',
    white: '#f1f1f1'
  },
  background: {
    default: '#161616'
  },
  grey: {
    '800': '#2C2C2C'
  }
};

//#0bf727 brand

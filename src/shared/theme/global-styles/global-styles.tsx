import {
  GlobalStyles as GlobalStylesMui,
  Interpolation,
  Theme
} from '@mui/material';

const styles: Interpolation<Theme> = {
  'a:visited': {
    color: 'inherit'
  },
  'a:hover': {
    color: 'inherit'
  },
  'a:active': {
    color: 'inherit'
  },
  a: {
    'text-decoration': 'none'
  }
};

export const GlobalStyles = () => {
  return <GlobalStylesMui styles={styles} />;
};

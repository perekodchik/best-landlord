import { TypographyOptions } from '@mui/material/styles/createTypography';

export const typographyOptions: TypographyOptions = {
  allVariants: {
    color: 'white'
  },
  fontFamily: [
    'Montserrat',
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    'Roboto',
    '"Helvetica Neue"',
    'Arial'
  ].join(',')
};

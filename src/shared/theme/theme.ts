import { createTheme } from '@mui/material';
import { typographyOptions } from './typography';
import { paletteOptions } from './palette';

export const theme = createTheme({
  typography: typographyOptions,
  palette: paletteOptions,
  components: {
    MuiDrawer: {
      styleOverrides: {
        paper: {
          backgroundColor: paletteOptions.grey?.['800']
        }
      }
    }
  }
});

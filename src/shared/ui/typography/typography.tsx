import {
  Typography as MaterialTypography,
  TypographyProps
} from '@mui/material';

export const Typography = (props: TypographyProps) => {
  return <MaterialTypography {...props} />;
};

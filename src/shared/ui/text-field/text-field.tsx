import {
  TextField as MaterialTextField,
  TextFieldProps,
  styled
} from '@mui/material';

export const StyledTextField = styled(MaterialTextField)(() => ({
  '& .MuiOutlinedInput-root': {
    border: '1px solid white',
    color: 'white'
  },
  '& .Mui-focused': {
    border: 'none'
  }
}));

export const TextField = (props: TextFieldProps) => {
  return <StyledTextField {...props} />;
};

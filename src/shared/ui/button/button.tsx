import { ButtonProps, Button as MaterialButton } from '@mui/material';

export const Button = (buttonProps: ButtonProps) => {
  return <MaterialButton {...buttonProps} />;
};

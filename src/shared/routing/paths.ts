export const paths = {
  root: '/',
  login: '/login',
  profile: '/profile',
  dashboard: '/dashboard',
  properties: {
    index: '/properties',
    add: '/properties/add'
  }
} as const;

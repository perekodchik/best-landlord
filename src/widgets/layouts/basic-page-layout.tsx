import { Box } from '@mui/material';
import { ReactNode } from 'react';
import { NavbarConnector } from '../navbar';

type Props = {
  children: ReactNode;
};

export const BasicPageLayout = ({}: Props) => {
  return (
    <Box padding={3}>
      <NavbarConnector />
    </Box>
  );
};

import { Box } from '@mui/material';
import { ReactNode } from 'react';
import { SidebarConnector, sidebarWidth } from '../sidebar';
import { NavbarConnector } from '../navbar';

type Props = {
  children: ReactNode;
};

export const PageLayout = ({ children }: Props) => {
  return (
    <Box display="flex">
      <SidebarConnector />
      <Box
        component="main"
        display="flex"
        flexDirection="column"
        flex="1"
        sx={{
          flexGrow: 1,
          p: 3,
          width: { sm: `calc(100% - ${sidebarWidth}px)` }
        }}
      >
        <NavbarConnector />
        {children}
      </Box>
    </Box>
  );
};

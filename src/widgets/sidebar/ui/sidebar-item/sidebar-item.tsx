import { Typography } from '@/shared/ui';
import { SidebarOption } from '@/widgets/types';
import { Box, useTheme } from '@mui/material';
import { Link } from '@tanstack/react-router';

type Props = SidebarOption;

export const SidebarItem = ({ Icon, label, link }: Props) => {
  const theme = useTheme();

  return (
    <Link to={link}>
      <Box display="flex" gap="8px" justifyContent="center" alignItems="center">
        <Icon htmlColor={theme.palette.common.white} />
        <Typography variant="button">{label}</Typography>
      </Box>
    </Link>
  );
};

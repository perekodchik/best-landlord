import { Box, Drawer, List, ListItem } from '@mui/material';
import { SidebarItem } from '../sidebar-item';
import { SidebarOption } from '../../../types';
import { sidebarWidth } from '../../constants';

type Props = {
  options: SidebarOption[];
};

export const Sidebar = ({ options }: Props) => {
  return (
    <Box
      component="nav"
      sx={{ width: { sm: sidebarWidth }, flexShrink: { sm: 0 } }}
    >
      <Drawer
        variant="permanent"
        sx={{
          display: { xs: 'none', sm: 'block' },
          '& .MuiDrawer-paper': { boxSizing: 'border-box', width: sidebarWidth }
        }}
        open
      >
        <Box
          p={2}
          display="flex"
          flexDirection="column"
          justifyContent="space-between"
          height="100%"
          alignItems="flex-start"
        >
          <List>
            {options.map((option) => (
              <ListItem key={option.link}>
                <SidebarItem {...option} />
              </ListItem>
            ))}
          </List>
          {/* <List>
            <ListItem>
              <SidebarItem Icon={LogoutIcon} label="logout" link="/logout" />
            </ListItem>
          </List> */}
        </Box>
      </Drawer>
    </Box>
  );
};

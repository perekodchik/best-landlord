import { sidebarOptions } from './options';
import { Sidebar } from './ui/sidebar/sidebar';

export const SidebarConnector = () => {
  return <Sidebar options={sidebarOptions} />;
};

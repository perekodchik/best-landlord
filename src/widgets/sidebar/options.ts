import { paths } from '@/shared/routing';

import BusinessCenterIcon from '@mui/icons-material/BusinessCenter';
import EuroIcon from '@mui/icons-material/Euro';
import AddIcon from '@mui/icons-material/Add';
import { SidebarOption } from '../types';

export const sidebarOptions: SidebarOption[] = [
  {
    label: 'Dashboard',
    link: paths.dashboard,
    Icon: BusinessCenterIcon
  },
  {
    label: 'Add Property',
    link: 'paths.properties.add',
    Icon: AddIcon
  },
  {
    label: 'Transactions',
    link: 'paths.transactions',
    Icon: EuroIcon
  }
];

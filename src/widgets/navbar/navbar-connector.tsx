import { UserInfo } from './types';
import { Navbar } from './ui/navbar';

export const NavbarConnector = () => {
  const userInfo: UserInfo = {
    name: 'Vladick',
    id: 'user-id-of-vladick',
    avatar: 'https://mui.com/static/images/avatar/1.jpg'
  };

  return <Navbar userInfo={userInfo} />;
};

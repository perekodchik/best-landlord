export type UserInfo = {
  name: string;
  avatar: string;
  id: string;
};

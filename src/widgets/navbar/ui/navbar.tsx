import { Avatar, Box } from '@mui/material';
import { UserInfo } from '../types';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
type Props = {
  userInfo: UserInfo;
};

export const Navbar = ({ userInfo: { avatar, name } }: Props) => {
  return (
    <Box
      component="nav"
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      width="100%"
      borderRadius={2}
      padding={1}
      marginBottom={3}
    >
      <ArrowBackIosIcon />
      <Avatar src={avatar} alt={name} />
    </Box>
  );
};

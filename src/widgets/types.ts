import { SvgIconComponent } from '@mui/icons-material';

export type SidebarOption = {
  link: string;
  label: string;
  Icon: SvgIconComponent;
};

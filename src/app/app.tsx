import { router } from '@/processes/routing';
import { GlobalStyles, theme } from '@/shared/theme';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { RouterProvider } from '@tanstack/react-router';

export function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline enableColorScheme />
      <GlobalStyles />
      <RouterProvider router={router} />
    </ThemeProvider>
  );
}

import { ForYouSection } from '@/entities/dashboard/for-you-section';
import { ForYouSectinItem } from '@/entities/dashboard/for-you-section/types';

export const ForYouSectionConnector = () => {
  const data: ForYouSectinItem[] = [
    {
      backgroundImage: 'https://www.w3schools.com/css/paris.jpg',
      link: 'https://www.w3schools.com/css/paris.jpg',
      title: 'Upcoming events',
      subtitle: '2023 overview'
    },
    {
      backgroundImage: 'https://www.w3schools.com/css/paris.jpg',
      link: 'https://www.w3schools.com/css/paris.jpg',
      title: 'Notifications',
      subtitle: 'Important'
    },
    {
      backgroundImage: 'https://www.w3schools.com/css/paris.jpg',
      link: 'https://www.w3schools.com/css/paris.jpg',
      title: 'Legal Information',
      subtitle: 'Learn More'
    }
  ];

  return <ForYouSection items={data} />;
};

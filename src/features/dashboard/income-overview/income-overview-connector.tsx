import { IncomeOverviewBlock } from '@/entities/dashboard';
import { MoneyAmount } from '@/shared/types';

export const IncomeOverviewConnector = () => {
  //useQuery
  const moneyAmount: MoneyAmount = {
    amount: 9876.77,
    currency: 'USD'
  };

  return <IncomeOverviewBlock moneyAmount={moneyAmount} />;
};

import { UpcomingEvents } from '@/entities/dashboard/upcoming-events';
import { EventMonthData } from '@/entities/dashboard/upcoming-events/types';

export const UpcomingEventsConnector = () => {
  const monthData: EventMonthData[] = [
    {
      month: 0,
      year: 2024,
      items: [
        {
          day: 10,
          link: 'google.com',
          title: 'Tenant payment',
          description: 'USER_NAME is expected to pay USD AMOUNT'
        },
        {
          day: 15,
          link: 'google.com',
          title: 'Interview',
          description: 'You have interview with new tenant',
          timeRange: '10:00-11:00'
        }
      ]
    },
    {
      month: 1,
      year: 2024,
      items: [
        {
          day: 10,
          link: 'google.com',
          title: 'Tenant payment',
          description: 'USER_NAME is expected to pay USD AMOUNT'
        },
        {
          day: 15,
          link: 'google.com',
          title: 'Interview',
          description: 'You have interview with new tenant',
          timeRange: '10:00-11:00'
        }
      ]
    },
    {
      month: 2,
      year: 2025,
      items: [
        {
          day: 10,
          link: 'google.com',
          title: 'Tenant payment',
          description: 'USER_NAME is expected to pay USD AMOUNT'
        },
        {
          day: 15,
          link: 'google.com',
          title: 'Interview',
          description: 'You have interview with new tenant',
          timeRange: '10:00-11:00'
        }
      ]
    }
  ];

  const allEventsLink = 'TODO: fix events link';

  return (
    <UpcomingEvents monthEvents={monthData} allEventsLink={allEventsLink} />
  );
};

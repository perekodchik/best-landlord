import { LoginForm } from '@/entities/login/ui';
import { Typography } from '@/shared/ui';
import { Box } from '@mui/material';

export const Login = () => {
  return (
    <Box display={'flex'}>
      <Box
        display={'flex'}
        justifyContent={'center'}
        alignItems={'center'}
        width={'100%'}
        minHeight={'100dvh'}
      >
        <Box
          display={'flex'}
          flexDirection={'column'}
          height={'100%'}
          justifyContent={'center'}
          alignItems={'center'}
          flex={'1'}
          bgcolor={'#2c2c2c'}
        >
          <img src="/images/login/house-locked.png" alt="house locked" />
          <Typography variant="h6" whiteSpace={'pre'} align="center">
            {
              'Your info is secure\n we never share your property data with third party'
            }
          </Typography>
        </Box>
        <LoginForm />
      </Box>
    </Box>
  );
};

import { HeroBanner } from '@/entities/landing/ui/hero-banner';

export const Landing = () => {
  return <HeroBanner />;
};

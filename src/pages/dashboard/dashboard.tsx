import { DashboardLayout } from '@/entities/dashboard/dashboard-layout';
import {
  ForYouSectionConnector,
  IncomeOverviewConnector,
  UpcomingEventsConnector
} from '@/features/dashboard';
import { Box } from '@mui/material';

export const Dashboard = () => {
  return (
    <DashboardLayout
      dashboard={
        <Box display="flex" flexDirection="column" gap={3} width="100%">
          <IncomeOverviewConnector />
          <ForYouSectionConnector />
        </Box>
      }
      rightDashboardSide={
        <Box display="flex" flexDirection="column" gap={3} width="100%">
          <UpcomingEventsConnector />
        </Box>
      }
    />
  );
};

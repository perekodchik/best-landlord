import { Router } from '@tanstack/react-router';
import { routeTree } from './routes';

export const router = new Router({
  routeTree
});

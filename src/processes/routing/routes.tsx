import {
  AddPropertyConnector,
  DashboardConnector,
  LandingConnector,
  LoginConnector
} from '@/pages';
import { Outlet, RootRoute, Route } from '@tanstack/react-router';
import { TanStackRouterDevtools } from './devtools';
import { paths } from '@/shared/routing';
import { BasicPageLayout, PageLayout } from '@/widgets/layouts';

export const rootRoute = new RootRoute({
  component: () => (
    <>
      <Outlet />
      <TanStackRouterDevtools />
    </>
  ),
  wrapInSuspense: true
});

export const pageLayoutRoutes = new Route({
  id: 'page-layout',
  getParentRoute: () => rootRoute,
  component: () => (
    <PageLayout>
      <Outlet />
    </PageLayout>
  )
});

export const basicPageLayoutRoutes = new Route({
  id: 'basic-page-layout',
  getParentRoute: () => rootRoute,
  component: () => (
    <BasicPageLayout>
      <Outlet />
    </BasicPageLayout>
  )
});

export const indexRoute = new Route({
  getParentRoute: () => rootRoute,
  path: paths.root,
  component: LandingConnector
});

export const loginRoute = new Route({
  getParentRoute: () => rootRoute,
  path: paths.login,
  component: LoginConnector
});

export const dashboardRoute = new Route({
  getParentRoute: () => pageLayoutRoutes,
  path: paths.dashboard,
  component: DashboardConnector
});

export const propertiesRoute = new Route({
  getParentRoute: () => rootRoute,
  path: paths.properties.index,
  component: () => (
    <div>
      "huy"
      <Outlet />
    </div>
  )
});

export const addPropertyRoute = new Route({
  getParentRoute: () => basicPageLayoutRoutes,
  path: paths.properties.add,
  component: AddPropertyConnector
});

export const routeTree = rootRoute.addChildren([
  pageLayoutRoutes.addChildren([dashboardRoute, addPropertyRoute]),
  basicPageLayoutRoutes.addChildren([addPropertyRoute]),
  indexRoute,
  loginRoute
]);

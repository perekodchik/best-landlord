import { Button, TextField, Typography } from '@/shared/ui';
import { Box } from '@mui/material';

export const LoginForm = () => {
  return (
    <Box
      padding={4}
      flex={'1'}
      display={'flex'}
      flexDirection={'column'}
      justifyContent={'center'}
      alignItems={'center'}
    >
      <Box
        display={'flex'}
        flexDirection={'column'}
        width={'100%'}
        maxWidth={400}
        gap={2}
      >
        <TextField
          color="primary"
          variant="outlined"
          label="login"
          placeholder="your_address@email.com"
        />
        <TextField label="password" placeholder="********" type="password" />
        <Button variant="contained">Login</Button>
        <Typography>Forgot your password?</Typography>
      </Box>
    </Box>
  );
};

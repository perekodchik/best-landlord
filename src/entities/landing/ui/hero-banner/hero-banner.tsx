import { Button } from '@/shared/ui';
import { Box, Typography } from '@mui/material';

export const HeroBanner = () => {
  return (
    <Box
      display={'flex'}
      justifyContent={'center'}
      alignItems={'center'}
      width={'100%'}
      minHeight={'100dvh'}
      paddingX={'10%'}
    >
      <Box flex={1} display={'flex'} flexDirection={'column'} gap={2}>
        <Typography variant="h2" component="h1" align="left">
          Connect with your rentees through{' '}
          <Typography variant="h2" component="span" color={'primary.main'}>
            BestLandlord
          </Typography>
        </Typography>
        <Box display={'flex'} gap={1}>
          <Button size="large" color="secondary" variant="contained">
            Sign up
          </Button>
          <Button size="large" color="primary" variant="outlined">
            Learn more
          </Button>
        </Box>
      </Box>
      <Box flex={1}>
        <img src="/images/landing/house.png" alt="house" />
      </Box>
    </Box>
  );
};

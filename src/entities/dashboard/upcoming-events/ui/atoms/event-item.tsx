import { Typography } from '@/shared/ui';
import { Box, useTheme } from '@mui/material';
import { Link } from '@tanstack/react-router';
import { EventItemData } from '../../types';

type Props = EventItemData;

export const EventItem = ({
  day,
  link,
  title,
  description,
  timeRange
}: Props) => {
  const theme = useTheme();

  return (
    <Link to={link}>
      <Box display="flex" gap={2}>
        <Box
          borderRadius={3}
          display="flex"
          alignItems="center"
          justifyContent="center"
          sx={{
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.common.white
          }}
          minWidth={64}
          height={64}
        >
          <Typography variant="h4" component="span">
            {day}
          </Typography>
        </Box>
        <Box display="flex" flexDirection="column" gap={1}>
          <Typography variant="subtitle1">{title}</Typography>
          <Typography variant="body1">{description}</Typography>
        </Box>
        <Box flex={1} />
        <Typography variant="caption">{timeRange}</Typography>
      </Box>
    </Link>
  );
};

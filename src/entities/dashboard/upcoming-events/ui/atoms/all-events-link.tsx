import { Button } from '@/shared/ui';
import { useTheme } from '@mui/material';
import { Link } from '@tanstack/react-router';

type Props = {
  link: string;
};

export const AllEventsLink = ({ link }: Props) => {
  const theme = useTheme();

  return (
    <Link to={link}>
      <Button
        variant="outlined"
        sx={{ borderColor: theme.palette.common.white }}
      >
        See all events
      </Button>
    </Link>
  );
};

import { Box } from '@mui/material';
import { EventMonthData } from '../../types';
import { EventMonth } from '../molecules';
import { AllEventsLink } from '../atoms';

type Props = {
  monthEvents: EventMonthData[];
  allEventsLink?: string;
};

export const UpcomingEvents = ({ monthEvents, allEventsLink }: Props) => {
  return (
    <Box display="flex" flexDirection="column" gap={1} alignItems="center">
      {monthEvents.map((monthEvent) => (
        <EventMonth key={monthEvent.month + monthEvent.year} {...monthEvent} />
      ))}
      {allEventsLink ? <AllEventsLink link={allEventsLink} /> : null}
    </Box>
  );
};

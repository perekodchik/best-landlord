import { Box, List, ListItem, Typography } from '@mui/material';
import { EventMonthData } from '../../types';
import { getMonthName, isCurrentYear } from '@/shared/utils/date';
import { EventItem } from '../atoms';

type Props = EventMonthData;

export const EventMonth = ({ items, month, year }: Props) => {
  const yearPostfix = !isCurrentYear(year) ? '' : ` ${year}`;

  return (
    <Box display="flex" flexDirection="column" gap={2}>
      <Typography variant="subtitle1" fontWeight={700}>
        {`${getMonthName(month)}${yearPostfix}`}
      </Typography>
      <List>
        {items.map((item) => (
          <ListItem key={item.link + item.title}>
            <EventItem {...item} />
          </ListItem>
        ))}
      </List>
    </Box>
  );
};

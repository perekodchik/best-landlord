export type EventItemData = {
  day: number;
  title: string;
  description?: string;
  link: string;
  timeRange?: string;
};

export type EventMonthData = {
  month: number;
  year: number;
  items: EventItemData[];
};

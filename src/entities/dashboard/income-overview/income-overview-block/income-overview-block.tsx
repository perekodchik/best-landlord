import { MoneyAmount } from '@/shared/types/types';
import { Typography } from '@/shared/ui';
import { Box, useTheme } from '@mui/material';

type Props = {
  moneyAmount: MoneyAmount;
};

export const IncomeOverviewBlock = ({ moneyAmount }: Props) => {
  const theme = useTheme();
  const moneyColor =
    moneyAmount.amount >= 0
      ? theme.palette.primary.main
      : theme.palette.warning.main;

  return (
    <Box
      bgcolor={theme.palette.grey['800']}
      borderRadius={3}
      component="section"
      padding={4}
      display="flex"
      width="100%"
      flexDirection="column"
      gap={1}
    >
      <Typography variant="h5">Income Overview</Typography>
      <Typography
        fontWeight="600"
        color={moneyColor}
        variant="h3"
        component="section"
      >
        {moneyAmount.currency + moneyAmount.amount}
      </Typography>
    </Box>
  );
};

import { Typography } from '@/shared/ui';
import { Box } from '@mui/material';
import { ReactNode } from 'react';

type Props = {
  dashboard: ReactNode;
  rightDashboardSide: ReactNode;
};

export const DashboardLayout = ({ dashboard, rightDashboardSide }: Props) => {
  return (
    <Box display="flex" gap={4} flexWrap="wrap">
      <Box
        display="flex"
        component="article"
        flex={2}
        gap={3}
        flexDirection="column"
      >
        <Typography component="h2" variant="h4">
          Dashboard
        </Typography>
        {dashboard}
      </Box>
      <Box
        display="flex"
        component={'article'}
        flex={1}
        flexBasis="200px"
        gap={3}
        flexDirection="column"
      >
        <Typography component="h2" variant="h4">
          Upcoming Events
        </Typography>
        {rightDashboardSide}
      </Box>
    </Box>
  );
};

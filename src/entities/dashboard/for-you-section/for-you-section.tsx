import { Box } from '@mui/material';
import { ForYouSectinItem } from './types';
import { ForYouItem } from './ui';
import { Button, Typography } from '@/shared/ui';

type Props = {
  items: ForYouSectinItem[];
};

export const ForYouSection = ({ items }: Props) => {
  return (
    <Box display="flex" flexDirection="column" gap={2} width="100%">
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Typography variant="h5">For You</Typography>
        <Button variant="outlined">See all</Button>
      </Box>
      <Box
        component="section"
        display="flex"
        gap={3}
        flexWrap="wrap"
        width="100%"
      >
        {items.map((item) => (
          <ForYouItem key={item.title} {...item} />
        ))}
      </Box>
    </Box>
  );
};

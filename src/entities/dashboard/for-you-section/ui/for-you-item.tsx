import { Box, useTheme } from '@mui/material';
import { ForYouSectinItem } from '../types';
import { Typography } from '@/shared/ui';

type Props = ForYouSectinItem;

export const ForYouItem = ({ title, subtitle }: Props) => {
  const theme = useTheme();
  return (
    <Box
      flex={1}
      component="li"
      borderRadius={3}
      boxShadow="0px 2px 12px 1px #ffffffcc"
      sx={{
        backgroundColor: theme.palette.primary.main,
        aspectRatio: 1
      }}
      display="flex"
      justifyContent="center"
      alignItems="center"
      position="relative"
      flexBasis="260px"
      maxHeight="180px"
    >
      <Box
        borderRadius={3}
        sx={{
          mixBlendMode: 'luminosity',
          filter: 'grayscale(50%)',
          opacity: 0.3,
          width: '100%',
          height: '100%',
          backgroundImage: 'url("https://www.w3schools.com/css/paris.jpg")',
          backgroundSize: 'cover'
        }}
      ></Box>
      <Box position="absolute" display="flex" flexDirection="column" gap={2}>
        <Typography
          component="div"
          variant="h5"
          fontWeight="600"
          align="center"
          color={theme.palette.common.black}
        >
          {title}
        </Typography>
        <Typography
          component="div"
          variant="h4"
          fontWeight="700"
          align="center"
          color={theme.palette.common.black}
        >
          {subtitle}
        </Typography>
      </Box>
    </Box>
  );
};

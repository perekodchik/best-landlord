export type ForYouSectinItem = {
  title: string;
  link: string;
  backgroundImage: string;
  subtitle?: string;
};
